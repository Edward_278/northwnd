﻿using FinalProject.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject.Core.Abstractions.Repositories
{
    public interface IOrderRepository : ISQLRepository<Order>
    {

       Array HighFreightCharges();
       Array HighFreightChargesOfYear();
       Array HighFreightChargesBetween();
       Array HighFreightChargesLastYear();
    }
}
