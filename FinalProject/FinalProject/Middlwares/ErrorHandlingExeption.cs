﻿using FinalProject.Core.Exeptions;
using Microsoft.AspNetCore.Http;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalProject.Middlwares
{

    public class ErrorHandlingExceptions
    {
        public class ErrorHandlingMiddleware
        {
            private RequestDelegate _next;

            public ErrorHandlingMiddleware(RequestDelegate next)
            {
                _next = next;
            }

            public async Task InvokeAsync(HttpContext httpContext)
            {
                try
                {
                    await _next(httpContext);
                }
                catch (LogicException ex)
                {
                    httpContext.Response.StatusCode = 400;
                    await httpContext.Response.WriteAsync(ex.Message);
                    Log.Logger.Error(ex, "");
                }
                catch (Exception ex)
                {
                    httpContext.Response.StatusCode = 500;
                    await httpContext.Response.WriteAsync(ex.Message);
                    Log.Logger.Error(ex, "");
                    throw;
                }
            }
        }
    }
}
