﻿using FinalProject.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject.Core.Abstractions.Repositories
{
    public interface ICustomerRepository : ISQLRepository<Customer>
    {
      Array  getCustomersByCountryAndCity();
      Array CustomerByRegion();
    }
}
