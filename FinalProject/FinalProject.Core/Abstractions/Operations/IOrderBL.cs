﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject.Core.Abstractions.Operations
{

    public interface IOrderBL
    {
       Array HighFreightCharges();
       Array HighFreightChargesOfYear();
       Array HighFreightChargesBetween();
       Array HighFreightChargesLastYear();

    }
}
