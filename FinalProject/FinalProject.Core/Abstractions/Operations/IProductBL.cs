﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject.Core.Abstractions.Operations
{
    public interface IProductBL
    {
      Array SortProducts();
      Array ProductsNeedReordering();
      Array ProductNeedReorderingContinueted();

    }
}
