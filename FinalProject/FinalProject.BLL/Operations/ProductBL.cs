﻿using FinalProject.Core.Abstractions;
using FinalProject.Core.Abstractions.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject.BLL.Operations
{
    public class ProductBL : IProductBL
    {
        private readonly IRepositoryManager _repositoryManager;
        public ProductBL(IRepositoryManager repositoryManager)
        {
            _repositoryManager = repositoryManager;
        }
        public Array SortProducts()
        {
            var SortedProducts = _repositoryManager.Products.SortProducts();
            return SortedProducts;
        }
        public Array ProductsNeedReordering()
        {
            return _repositoryManager.Products.ProductsNeedReordering();
        }
        public Array ProductNeedReorderingContinueted()
        {
            return _repositoryManager.Products.ProductNeedReorderingContinueted();
        }
     
    }
}
