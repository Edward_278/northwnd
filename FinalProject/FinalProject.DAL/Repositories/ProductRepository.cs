﻿using FinalProject.Core.Abstractions.Repositories;
using FinalProject.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject.DAL.Repositories
{
    public class ProductRepository : SqlRepositoryBase<Product>, IProductRepository
    {
        private readonly NORTHWNDContext _context;
        public ProductRepository(NORTHWNDContext dbContext) : base(dbContext)
        {
            _context = dbContext;
        }
      

        public Array SortProducts()
        {
            var result = _context.Products
                                 .Join(_context.Categories,
                                 x => x.CategoryId, y => y.CategoryId,
                                 (x, y) => new
                                 { x.CategoryId, y.CategoryName, x.ProductId })
                                 .GroupBy(x => new { x.CategoryId, x.CategoryName })
                                 .Select(x => new { CategoryName = x.Key.CategoryName, TotalProducts = x.Count() })
                                 .OrderByDescending(x => x.CategoryName)
                                 .ToArray();
            return result;
        }
    
        public Array ProductsNeedReordering()
        {
            var result = _context.Products
                                 .Where(x => x.UnitsInStock < x.ReorderLevel)
                                 .OrderBy(x => x.ProductId)
                                 .Select(x => new { x.ProductName, x.UnitsInStock, x.UnitsOnOrder, x.ReorderLevel, x.Discontinued })
                                 .ToArray();

            return result;
        }
      
        public Array ProductNeedReorderingContinueted()
        {
            var result = _context.Products
                                 .Where(x => x.UnitsInStock + x.UnitsOnOrder <= x.ReorderLevel && x.Discontinued == false)
                                 .OrderBy(x => x.ProductId)
                                 .Select(x => new { x.ProductName, x.UnitsInStock, x.UnitsOnOrder, x.ReorderLevel, x.Discontinued })
                                 .ToArray();
            return result;
        }
    }
}
