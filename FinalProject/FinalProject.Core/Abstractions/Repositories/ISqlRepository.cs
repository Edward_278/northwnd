﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject.Core.Abstractions.Repositories
{
   public interface ISQLRepository<T> where T:class
    {
        T Get(int id);
        void Edit(T entity);
        void Remove(T entity);
        T Add(T entity);
        IEnumerable<T> GetWhere(Func<T, bool> predicate);
    }
}
