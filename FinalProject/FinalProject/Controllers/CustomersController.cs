﻿using FinalProject.Core.Abstractions.Operations;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalProject.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerBL _customerBL;
        public CustomersController(ICustomerBL customerBL)
        {
            _customerBL = customerBL;
        }
        [HttpGet("Count")]
        public IActionResult getCustomersByCountryAndCity()
        {

            return Ok(_customerBL.getCustomersByCountryAndCity());
        }
        [HttpGet("Region")]
        public IActionResult CustomerByRegion()
        {
            return Ok(_customerBL.CustomerByRegion());
        }
    }
}

