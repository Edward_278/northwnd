﻿using FinalProject.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject.Core.Abstractions.Repositories
{
    public interface IProductRepository : ISQLRepository<Product>
    {
        Array SortProducts();
        Array ProductsNeedReordering();
        Array ProductNeedReorderingContinueted();
    }
}
